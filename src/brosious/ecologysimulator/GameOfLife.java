package brosious.ecologysimulator;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class GameOfLife extends JFrame{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	TorusBoard board;
	private int delay;
	private int iteration = 0;
	private int totalIter = 0;
	private boolean simRunning = false;
	public final static String[] labelText = {"Generation: "};
	public final static String[] startBoards ={"gosper.txt","pulsar.txt","spaceships.txt","infile.txt"};
	/**
	 * Instantiate new GameOfLife object with give height and width.
	 * Sets up TorusBoard as well as appropriately sized JFrame window.
	 * @param height
	 * @param width
	 */
	GameOfLife(int width, int height) {
		board = new TorusBoard(width,height);
		this.init(width,height);
	}
	
	GameOfLife(int width, int height, ArrayList<ArrayList<TorusTile>> arr){
		board = new TorusBoard(width,height, arr);
		this.init(width,height);
	}
	
	/**
	 * Initialize object with JFrame+JPanel
	 * @param width
	 * @param height
	 */
	private void init(int width, int height){
		TorusTile.setTileSize((int) (500 / height));
		setSize((height*TorusTile.tileSize()) + 100, (width*TorusTile.tileSize()) + 100);
		setPreferredSize(new Dimension((height*TorusTile.tileSize()) + 100, (width*TorusTile.tileSize()) + 100));
		setTitle("Game of Life");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLayout(new BorderLayout());
		setLocation(300,300);
		setResizable(false);
		JPanel theJPanel = new JPanel(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				for(int i=0; i<board.height(); i++){
					for (int j=0; j<board.width(); j++){
						g.setColor(TorusTile.getColor(board.getTile(j, i).type()));
						g.fillRect(50+(j*TorusTile.tileSize()), 50+(i*TorusTile.tileSize()), TorusTile.tileSize(), TorusTile.tileSize());
						g.setColor(Color.BLACK);
						if (totalIter == 0) g.drawString(GameOfLife.labelText[0]+Integer.toString(iteration+1), 50, 25);
						else g.drawString(GameOfLife.labelText[0]+Integer.toString(iteration+1)+"/"+Integer.toString(totalIter), 50, 25);
						if (totalIter-1 == iteration) g.drawString("Simulation Complete!", 50, 40);
					}
				}
			}
		};
		JButton runButton = new JButton("Start");
		JButton closeButton = new JButton("Close");
		runButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				simRunning = !simRunning;
				JButton s = (JButton) e.getSource();
				if (!simRunning) s.setText("Start");
				else s.setText("Pause");
			}
		});
		closeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				GameOfLife.this.closeWindow();
			}
		});
		theJPanel.add(runButton).validate();
		theJPanel.add(closeButton);
		add(theJPanel, BorderLayout.CENTER);
		pack();
		setVisible(true);
	}
	
	/**
	 * Get the TorusBoard object.
	 * @return <i>TorusBoard</i> board
	 */
	public TorusBoard getBoard(){return this.board;}
	/**
	 * Set <i>TorusTile</i> at given location to given state.
	 * @param x
	 * @param y
	 * @param state
	 */
	public void setTile(int x, int y, int state){this.board.setTile(x, y, state);}
	public int delay(){return this.delay;}
	public void setDelay(double d){this.delay = (int) (d*1000);}
	public void iterate(){++this.iteration;}
	public int iterations(){return this.iteration;}
	public void setTotalIterations(int i){this.totalIter = i;}
	public int totalIterations(){return this.totalIter;}
	public boolean isRunning(){return this.simRunning;}
	public void closeWindow(){
		WindowEvent wev = new WindowEvent(this, WindowEvent.WINDOW_CLOSING);
		Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
	}
	public static void main(String[] args) throws InterruptedException {
		// Dialog choices for starting board state.
		Object[] choices = {
				"Predefined - Gosper's Glider Gun",
				"Predefined - Pulsar",
				"Predefined - Spaceships",
				"Use \"infile.txt\"",
				"Random Board"
		};
		String startChoice = (String) JOptionPane.showInputDialog(null,"Choose initial board state.","Game Of Life", JOptionPane.PLAIN_MESSAGE,null,choices,"-Choose One-");
		//protect against hitting Cancel
		if (startChoice != null){
			//Get index of selected board state.
			int cIndex=0;
			while(startChoice != choices[cIndex]) cIndex++;
			
			GameOfLife gameBoard = null;
			ArrayList<ArrayList<TorusTile>> boardArray = new ArrayList<ArrayList<TorusTile>>(0);
	
			if (cIndex < 4){
				BufferedReader reader;
				try {
					String line = null;
					reader = new BufferedReader(new FileReader(GameOfLife.startBoards[cIndex]));
					//Convert file to 2D arraylist.
					while ((line = reader.readLine()) != null){
						ArrayList<TorusTile> inList = new ArrayList<TorusTile>(0);
						String[] parts = line.split("");
						for (String part:parts){
							if (part.equals(".")){
								inList.add(new TorusTile(TorusTile.TYPE_EMPTY));
							}
							else if (part.equals("#")){
								inList.add(new TorusTile(TorusTile.TYPE_FILLED));
							}
							else{
								inList.add(new TorusTile(TorusTile.TYPE_EMPTY));
							}
						}
						boardArray.add(inList);
						inList = new ArrayList<TorusTile>(0);
					}
					reader.close();
				} catch (FileNotFoundException e) {
					System.out.println(e);
					e.printStackTrace();
				} catch (IOException e) {
					System.out.println(e);
					e.printStackTrace();
				}
				int w = boardArray.get(0).size();
				int h = boardArray.size();
				gameBoard = new GameOfLife(h,w,boardArray);
			}
			else {
				//Prompt board height
				String rh = null;
				rh = (String) JOptionPane.showInputDialog(null,"Specify board height:","Game Of Life",JOptionPane.PLAIN_MESSAGE,null,null,"100");
				//protect against hitting Cancel
				if (rh != "" && rh != null) {
					int h = Integer.parseInt(rh);
					//prompt board width
					String rw = null;
					rw = (String) JOptionPane.showInputDialog(null,"Specify board width:","Game Of Life",JOptionPane.PLAIN_MESSAGE,null,null,"100");
					//protect against hitting Cancel
					if (rw != "" && rw != null){
						int w = Integer.parseInt(rw);
						gameBoard = new GameOfLife(h,w);
					}
				}
			}
			//protect against hitting Cancel - from previous clause
			if (gameBoard != null){
			//prompt number of generations
				String rg = null;
				rg = (String) JOptionPane.showInputDialog(null,"Specify number of generations (0 = infinite):","Game Of Life",JOptionPane.PLAIN_MESSAGE,null,null,"0");
				//protect against hitting Cancel
				if (rg != "" && rg != null) {
					gameBoard.setTotalIterations(Integer.parseInt(rg));
					
					// Create the board and the window it will be displayed in.
					gameBoard.repaint();
					gameBoard.setDelay(0.1);
					
					// Game loop.
					while(true){
						while (!gameBoard.isRunning()){Thread.sleep(5);}//do fuck all
						evaluate(gameBoard);
						if (gameBoard.totalIterations() != 0){
							if (gameBoard.iterations() >= gameBoard.totalIterations()) break;
						}
						gameBoard.repaint();
						Thread.sleep(gameBoard.delay());
					}
				}
				else {
					// close the window if it already opened but an empty string was
					// returned from the generation prompt or it was cancelled.
					gameBoard.closeWindow();
				}
			}
		}
	}
	
	/**
	 * Evaluates game board to Game of Life rules.
	 * @param g
	 * @return 
	 */
	private static void evaluate(GameOfLife g){
		TorusBoard newg = new TorusBoard(g.getWidth(),g.getHeight());
		for(int i=0;i<g.getBoard().width();i++){
			for(int j=0;j<g.getBoard().height();j++){
				if (g.getBoard().getTile(i, j).type() == TorusTile.TYPE_EMPTY){
					if (g.getBoard().getNeighborsInState(TorusTile.TYPE_FILLED, i, j) == 3) {
						// Reproduction
						newg.setTile(i, j, TorusTile.TYPE_FILLED);
					}
					else newg.setTile(i,j, g.getBoard().getTile(i, j).type());
				}
				else if (g.getBoard().getTile(i, j).type() == TorusTile.TYPE_FILLED) {
					if (g.getBoard().getNeighborsInState(TorusTile.TYPE_FILLED, i, j) < 2){
						// Under population
						newg.setTile(i, j, TorusTile.TYPE_EMPTY);
					}
					else if (g.getBoard().getNeighborsInState(TorusTile.TYPE_FILLED, i, j) > 3) {
						// Over population
						newg.setTile(i, j, TorusTile.TYPE_EMPTY);
					}
					else newg.setTile(i,j, g.getBoard().getTile(i, j).type());
				}
				
			}
		}
		g.getBoard().setBoard(newg.getBoard());
		g.iterate();
	}
}
