package brosious.ecologysimulator;

import java.util.ArrayList;

public class TorusBoard {
	public static final int NORTH = 1;
	public static final int NORTH_EAST = 2;
	public static final int EAST = 3;
	public static final int SOUTH_EAST = 4;
	public static final int SOUTH = 5;
	public static final int SOUTH_WEST = 6;
	public static final int WEST = 7;
	public static final int NORTH_WEST = 8;

	private int height, width;
	private ArrayList<ArrayList<TorusTile>> board = new ArrayList<ArrayList<TorusTile>>(0);

	/**
	 * Instantiate TorusBoard with height and width. Randomizes starting grid.
	 * @param height
	 * @param width
	 */
	public TorusBoard(int h, int w) {
		height = h;
		width = w;
		ArrayList<TorusTile> innerList = new ArrayList<TorusTile>(0);
        for (int j = 0; j < h; j++) {
            for (int i = 0; i < w; i++) {
                innerList.add(new TorusTile());
            }
            board.add(innerList);
            innerList = new ArrayList<TorusTile>(0);
        }
	}
	
	public TorusBoard(int h, int w, ArrayList<ArrayList<TorusTile>> arr){
		height = h;
		width = w;
		board = arr;	
	}
	
	public void setBoard(ArrayList<ArrayList<TorusTile>> arr){this.board = arr;}

	/**
	 * Sets tile state (type) in grid.
	 * @param x_coord
	 * @param y_coord
	 * @param state
	 */
	public void setTile(int x, int y, int state){board.get(y).get(x).setType(state);}
	/**
	 * returns integer of board height.
	 * @return<i>int</i> height
	 */
	public int height() {return this.height;}
	/**
	 * Returns integer of board width.
	 * @return <i>int</i> width
	 */
	public int width() {return this.width;}
	/**
	 * Returns raw data <i>ArrayList</i>.
	 * @return <i>ArrayList</i> board
	 */
	public ArrayList<ArrayList<TorusTile>> getBoard(){return this.board;}
	/**
	 * Returns TorusTile object.
	 * @param x_coord
	 * @param y_coord
	 * @return <i>TorusTile</i> tile
	 */
	public TorusTile getTile(int x, int y){return this.board.get(y).get(x);}
	/**
	 * Get the neighboring cell in given direction starting at given Tile's coordinates.
	 * @param dir
	 * @param oX
	 * @param oY
	 * @return <i>TorusTile</i> tile
	 */
	public TorusTile getNeighbor(int dir, int oX, int oY){
		// TODO: Use modulus to more efficiently do this. 
		if (dir == TorusBoard.NORTH) {
			if (oY == 0) return this.board.get(this.height-1).get(oX);
			else return this.board.get(oY-1).get(oX);
		}
		else if (dir == TorusBoard.NORTH_EAST) {
			if ((oY == 0) && (oX == (this.width-1))) return this.board.get(this.height-1).get(0);
			else if (oY == 0) return this.board.get(this.height-1).get(oX+1);
			else if (oX == (this.width-1)) return this.board.get(oY-1).get(0);
			else return this.board.get(oY-1).get(oX+1);
		}
		else if (dir == TorusBoard.NORTH_WEST) {
			if ((oY == 0) && (oX == 0)) return this.board.get(this.height-1).get(this.width-1);
			else if (oY == 0) return this.board.get(this.height-1).get(oX-1);
			else if (oX == 0) return this.board.get(oY-1).get(this.width-1);
			else return this.board.get(oY-1).get(oX-1);
		}
		else if (dir == TorusBoard.SOUTH) {
			if (oY == (this.height-1)) return this.board.get(0).get(oX);
			else return this.board.get(oY+1).get(oX);
		}
		else if (dir == TorusBoard.SOUTH_EAST) {
			if ((oY == (this.height-1)) && (oX == (this.width-1))) return this.board.get(0).get(0);
			else if (oX == (this.width-1)) return this.board.get(oY+1).get(0);
			else if (oY == (this.height-1)) return this.board.get(0).get(oX+1);
			else return this.board.get(oY+1).get(oX+1);
		}
		else if (dir == TorusBoard.SOUTH_WEST) {
			if ((oY == (this.height-1)) && (oX == 0)) return this.board.get(0).get(this.height-1);
			else if (oX == 0) return this.board.get(oY+1).get(this.width-1);
			else if (oY == (this.height-1)) return this.board.get(0).get(oX-1);
			else return this.board.get(oY+1).get(oX-1);
		}
		else if (dir == TorusBoard.EAST) {
			if (oX == (this.width-1)) return this.board.get(oY).get(0);
			else return this.board.get(oY).get(oX+1);
		}
		else if (dir == TorusBoard.WEST) {
			if (oX == 0) return this.board.get(oY).get(this.width-1);
			else return this.board.get(oY).get(oX-1);
		}
		else return null;
	}

	/**
	 * Get number of neighbors in given state around given coordinates.
	 * @param state
	 * @param oX
	 * @param oY
	 * @return <i>int</i> number
	 */
	public int getNeighborsInState(int state, int oX, int oY){

		int neighborsState = 0;
		if (this.getNeighbor(TorusBoard.EAST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.WEST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.NORTH, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.SOUTH, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.NORTH_EAST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.SOUTH_EAST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.NORTH_WEST, oX, oY).type() == state){
			neighborsState++;
		}
		if (this.getNeighbor(TorusBoard.SOUTH_WEST, oX, oY).type() == state){
			neighborsState++;
		}
		return neighborsState;
	}
}
