package brosious.ecologysimulator;

import java.awt.Color;

public class Lumberjack extends TorusTile {

	private int xPos;
	private int yPos;
	private boolean alive = true;
	private boolean canMove = true;
	private int woodCount = 0;
	private int moveNum = 0;
	private String name;
	public static final Color TILE_COLOR = Color.ORANGE;
	public static final String[] NAMES_LIST = {
		"Bob","Chris","George","Ben","Theodore","Jamie","Steve"
		,"Eric","Hodor","Tyrion","Lawrence","Greg","Romeo","Luke"
		,"Brian","Martin","Robert","Peter","Mike","Micheal","Pablo"
		,"Larry","Thomas","Tom","Jared","Alex","James","Jim","Ian"
	};
	
	public Lumberjack(){
		this.alive=false;
	}
	
	public Lumberjack(int x, int y) {
		this.xPos = x;
		this.yPos = y;
		this.name = NAMES_LIST[TorusTile.genInt(0, NAMES_LIST.length-1)];
	}

	public int xPos(){return this.xPos;}
	public void setX(int x){this.xPos = x;}
	public int yPos(){return this.yPos;}
	public void setY(int Y){this.yPos = Y;}
	public boolean canMove(){return this.canMove;}
	public void stop(){this.canMove = false;}
	public boolean hasMoves(){return (moveNum < 3);}
	public void remove(){this.stop();this.alive = false;}
	public boolean isAlive(){return this.alive;}
	public String name(){return this.name;}
	public void chop(int w){this.woodCount = this.woodCount + w;}
	public void chop(){this.woodCount++;}
	public void move(int x, int y){
		if (this.canMove() && this.hasMoves()){
			this.xPos = x;
			this.yPos = y;
			this.moveNum++;
		}
	}
}
