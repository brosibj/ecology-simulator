package brosious.ecologysimulator;

import java.awt.Color;

public class Bear extends TorusTile {

	private int xPos;
	private int yPos;
	private boolean alive = true;
	private boolean canMove = true;
	private int moveNum = 0;
	public static final Color TILE_COLOR = Color.BLACK;
	
	public Bear() {
		this.alive = false;
	}
	public Bear(int x, int y) {
		this.xPos = x;
		this.yPos = y;
	}

	public int xPos(){return this.xPos;}
	public void setX(int x){this.xPos = x;}
	public int yPos(){return this.yPos;}
	public void setY(int Y){this.yPos = Y;}
	public boolean canMove(){return this.canMove;}
	public void stop(){this.canMove = false;}
	public boolean hasMoves(){return (moveNum < 5);}
	public void remove(){this.stop();this.alive = false;}
	public boolean isAlive(){return this.alive;}
	public void move(int x, int y){
		if (this.canMove() && this.hasMoves()){
			this.xPos = x;
			this.yPos = y;
			this.moveNum++;
		}
	}
}
