package brosious.ecologysimulator;

import java.awt.Color;
import java.util.Random;

public class TorusTile {

	public static Random r = new Random();
	public static final int TYPE_EMPTY = 0;
	public static final int TYPE_FILLED = 1;
	public static final Color COLOR_EMPTY = Color.BLACK;
	public static final Color COLOR_FILLED = Color.GREEN;
	
	private int type;
	private static int tileSize = 5;
	private static final Color[] colorTypes = {COLOR_EMPTY,COLOR_FILLED};
	
	/**
	 * Instantiate TorusTile with a type.
	 * 
	 * @param type
	 */
	public TorusTile(int type){
		this.type = type;
	}
	
	/**
	 * Instantiate TorusTile with a random type.
	 */
	public TorusTile() {
        int theRandTile = TorusTile.genInt(0, colorTypes.length-1);
		this.type = theRandTile;
	}

	/**
	 * Set the TorusTile type.
	 * @param state
	 */
	public void setType(int state){this.type = state;}
	/**
	 * Get the TorusTile type.
	 * @return <i>int</i> type
	 */
	public int type() {return this.type;}
	
	/**
	 * Get Color of specific type of tile.
	 * @param type
	 * @return <i>Color</i> color
	 */
	public static Color getColor(int type) {return TorusTile.colorTypes[type];}
	
	public static int tileSize(){return tileSize;}
	public static void setTileSize(int s){tileSize = s;}
	public static int genInt(int min, int max){return TorusTile.r.nextInt((max-min)+1)+min;}
}
