package brosious.ecologysimulator;

import java.awt.Color;

public class Tree extends TorusTile {

	private int xPos;
	private int yPos;
	private int type = 0;
	private boolean alive = true;
	private int age = 0;
	public static final Color TILE_SAPLPING_COLOR = new Color(0,200,0);
	public static final Color TILE_TREE_COLOR = new Color(0,150,0);
	public static final Color TILE_ELDER_COLOR = new Color(0,100,0);
	public static final int TYPE_SAPLING = 0;
	public static final int TYPE_TREE = 1;
	public static final int TYPE_ELDER = 2;
	
	public Tree() {
		this.alive=false;
	}
	public Tree(int x, int y) {
		this.xPos = x;
		this.yPos = y;
		this.type = Tree.TYPE_TREE;
	}
	public Tree(int x, int y, int type) {
		this.xPos = x;
		this.yPos = y;
		this.type = type;
	}
	
	public int xPos(){return this.xPos;}
	public int yPos(){return this.yPos;}
	public int type(){return this.type;}
	public boolean isType(int t){return (this.type == t);}
	public int grow(){return ++this.type;}
	public int state(){return this.type;}
	public int age(){return this.age;}
	public void chop(){this.alive = false;}
	public boolean isAlive(){return this.alive;}
	public boolean doesSpawn(){
		int chance = 10; //1 in 10 (10%)
		if (this.state() == Tree.TYPE_ELDER) chance = 5; //1 in 5 (20%)
		int rn = TorusTile.genInt(0, chance);
		return (rn == 2);
	}
	public int tick(){
		if (this.isAlive()) {
			++this.age;
			if (this.state() == Tree.TYPE_SAPLING && this.age() == 12) this.grow();
			else if (this.state() == Tree.TYPE_TREE && this.age() == 120) this.grow();
			return this.age;
		}
		else {
			return -1;
		}
	}
	

}
