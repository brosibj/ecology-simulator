package brosious.ecologysimulator;

import java.util.ArrayList;

public class Forest extends TorusBoard {
	
	private ArrayList<ArrayList<Object>> tiles;

	public Forest(int h, int w) {
		super(h, w);
		init(h,w);
	}

	public Forest(int h, int w, ArrayList<ArrayList<TorusTile>> arr) {
		super(h, w, arr);
		init(h,w);
	}

	private void init(int h, int w){
		ArrayList<Object> inTiles = new ArrayList<Object>(0);
		for (int j = 0; j < h; j++) {
			for (int i = 0; i < w; i++) {
				inTiles.add(new Object());
			}
			tiles.add(inTiles);
			inTiles = new ArrayList<Object>(0);
		}
	}
	public int numType(Object c){
		int n = 0;
		for (int j = 0; j < this.height(); j++){
			for (int i = 0; i < this.width(); i++){
				if (tiles.get(j).get(i).getClass() == c) n++;
			}
		}
		return n;
	}
	public void addLumberJacks(int p){
		int n = (this.height()*this.width()) * (p/100);
		while (numType(Lumberjack.class) < n){
			int x = TorusTile.genInt(0, this.width());
			int y = TorusTile.genInt(0, this.height());
			tiles.get(y).set(x, new Lumberjack(x,y));
		}
	}
	public void addBears(int p){
		int n = (this.height()*this.width()) * (p/100);
		while (numType(Bear.class) < n){
			int x = TorusTile.genInt(0, this.width());
			int y = TorusTile.genInt(0, this.height());
			tiles.get(y).set(x, new Bear(x,y));
		}
	}
	public void addTrees(int p){
		int n = (this.height()*this.width()) * (p/100);
		while (numType(Tree.class) < n){
			int x = TorusTile.genInt(0, this.width());
			int y = TorusTile.genInt(0, this.height());
			tiles.get(y).set(x, new Tree(x,y));
		}
	}
	
	public void tick(Lumberjack l){
		while(l.canMove()){
			Object nextTile = this.getNeighbor(TorusTile.genInt(1, 8), l.xPos(), l.yPos());
			if (nextTile.getClass() == Tree.class){
				Tree itsATree = (Tree) nextTile;
				if (itsATree.isType(Tree.TYPE_TREE) || itsATree.isType(Tree.TYPE_ELDER)){
					itsATree.chop();
					l.chop(itsATree.type());
					l.stop();
				}
			}
		}
	}
}
